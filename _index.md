---
layout: base
title: ReverseEagle
permalink: /index.html
---

# Welcome to ReverseEagle!

ReverseEagle is a young community thriving to stop monolithic organizations taking over the web and the software we use, for the sake of our privacy, security, freedom and health.

Currently, we are dedicated to helping developers with removing or replacing the unnecessary proprietary code in their open source projects, and also helping *you* find free and open-source alternatives to everyday software and services.

## Helpful resources

- [switching.software](https://switching.software)
- [tycrek/degoogle](https://github.com/tycrek/degoogle)

## ReverseEagle Team

- [Resynth](https://resynth1943.net)
- [TheMainOne](https://floss.social/@TheMainOne)
- [taminaru](https://dev.lemmy.ml/u/taminaru)
- [penloy](https://penloy.com)

{% include posts_list.html %}