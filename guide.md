---
layout: post
title: ReverseEagle - How To's
---

# Guide

If you're a developer and you wish to help this movement, you *could* dedicate 5 minutes of your time to removing Google's influence from a FOSS project. For example, a good place to start would be removing Google's algorithmic analytics, or any other Google functionality.

Every project cleansed of Google's control is a victory for privacy.

[Here's a list of sites that contain Google integration.](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/src/branch/master/MERGE_REQUESTS.md) 

When you've finished your Pull Request, send one in to us! [Add your merge request to our list!](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/src/branch/master/MERGE_REQUESTS.md)

## Pull Requests

In most scenarios, you'll want to open a Pull Request that removes Google's functionality from a certain FOSS project. To help you out, we have some templates below for the Pull Request message.

### Template for websites

If you're removing Google from a website with a merge request, this is the template you'll want to use.

<code>
Would you be willing to remove this Google Analytics 'functionality' from this website? There are better alternatives if you _must_ keep tabs on users' activity.
<br><br>
Some reputable alternatives can be seen here: https://switching.software/replace/google-analytics/
</code>
    
### Template for themes

If you're removing Google from a Jekyll theme, for example, this template would be more appropriate.

<code>
Would you be willing to remove this Google Analytics 'functionality' from this theme? There are better alternatives if you _must_ keep tabs on users' activity.
<br><br>
Some reputable alternatives can be seen here: https://switching.software/replace/google-analytics/
</code>
